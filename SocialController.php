<?php
declare(strict_types=1);

namespace App\Http\Controllers;


use App\Services\SocialAccount\SocialAccountContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;

/**
 * Class SocialController
 * @package App\Http\Controllers
 */
class SocialController extends Controller
{
    /**
     * @var SocialAccountContract
     */
    protected $socialService;

    /**
     * SocialController constructor.
     * @param SocialAccountContract $socialAccountContract
     */
    public function __construct(SocialAccountContract $socialAccountContract)
    {
        $this->socialService = $socialAccountContract;
    }

    /**
     * @param $provider
     * @return mixed
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleProviderCallback($provider)
    {
        try {
            if ('twitter' !== $provider) {
                $user = Socialite::driver($provider)->stateless()->user();
            } else {
                $user = Socialite::driver($provider)->user();
            }
        } catch (\Exception $exception) {
            return redirect(route('login'));
        }

        $auth_user = $this->socialService->findOrCreateUser($user, $provider);
        Session::put('socialProvider', $provider);
        Auth::login($auth_user);

        $route_name = Session::get('redirect', 'tutor.tutorDashboard');

        return redirect(route($route_name));
    }

}
