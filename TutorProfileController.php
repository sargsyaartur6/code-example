<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\TutorRequest;
use App\Services\TutorField\TutorFieldContract;
use App\Services\User\UserContract;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

/**
 * Class TutorProfileController
 * @package App\Http\Controllers
 */
class TutorProfileController extends Controller
{
    /**
     * @var
     */
    protected $tutorProfileContract;

    /**
     * @var TutorFieldContract
     */
    protected $tutorFieldContract;

    /**
     * @var UserContract
     */
    protected $userContract;


    /**
     * TutorProfileController constructor.
     * @param TutorFieldContract $tutorFieldContract
     * @param UserContract $userContract
     */
    public function __construct(
        TutorFieldContract $tutorFieldContract,
        UserContract $userContract
    ) {
        $this->tutorFieldContract = $tutorFieldContract;
        $this->userContract = $userContract;
    }

    /**
     * @return View
     */
    public function tutorDashboard(): View
    {
        return view('tutor.profile.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProfile()
    {
        try {
            $tutor_fields = $this->tutorFieldContract->getTutorWithParams();

            return view('tutor.profile.components.basic-profile')->with([
                'tutorFields' => $tutor_fields,
            ]);
        } catch (\Exception $exception) {
            return response()->json(['message' => __('messages.something_went_wrong')], 500);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProfileMessages()
    {
        return view('tutor.profile.components.messages');
    }

    /**
     * @param TutorRequest $request
     * @return Response
     */
    public function signupTutor(TutorRequest $request): Response
    {
        $fields = collect($request->input('fields'));
        $name_field_id = $this->tutorFieldContract->getTitleByFullName();

        $edited = $this->userContract->updateRole($fields->where('id', '=', $name_field_id)->first()['value'], $fields,
            $request->input('percent'));

        if (!$edited) {
            return response(['message' => __('messages.something_went_wrong')], 500);
        }

        return response(['success' => true]);
    }

    /**
     * @TODO
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function imageUpload(Request $request)
    {
        $save_image = $this->tutorProfileContract->profileImageSave($request->file('image'));

        if (!$save_image) {
            return redirect()->back();
        }

        return view('tutor');
    }

    public function getPercent()
    {
        return \Auth::user()->profile_percent;
    }
}
